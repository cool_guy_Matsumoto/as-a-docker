#+TITLE:docker-composeについて

* docker-composeコマンド
引数には、コンテナ名を入れる。
入れなければ、すべてのコンテナについてコマンドが実行される。
#+BEGIN_SRC bash
#Dockerfileをもとにイメージのビルド&コンテナの立ち上げ
$ docker-compose up
#定義ファイルがdocker-compose.yaml以外のファイル名のときは、
#-fオプションでファイル名を指定する
$ docker-compose -f ./dirP/dirC/docker-compose.yaml up
#opsion
--build
--no-build
--timeout
-d
--scale SERVICE名=サービス数
--no-deps

#コンテナの一覧
$ docker-compose ps

#複数のdockerコンテナの停止
$ docker-compose stop

#docker-composeのバージョン確認
$ docker-compose --version

#コンテナのログ出力
$ docker-compose logs

#コンテナの実行
$ docker-compose run

#コンテナの起動
$ docker-compose start

#コンテナの停止
$ docker-compose stop

#実行中のコンテナを強制停止する
$ docker-compose kill

#コンテナの再起動
$ docker-compose restat

#コンテナの消去
$ docker-compose rm

#docker-compose.yamrで定義したリソース(コンテナ、ネットワーク、イメージ)を除去
$ docker-compose down

#コンテナの中断
$ docker-compose pause

#コンテナの再開
$ docker-compose unpause

#コンテナの公開ポート番号表示
$ docker-compose port

#コンテナの構成を確認
$ docker-compose config


#+END_SRC
