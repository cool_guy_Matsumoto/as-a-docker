#+TITLE:Dockerfileの書き方
公式ドキュメント: [[http://docs.docker.jp/engine/reference/builder.html#id2]]
* 書き方 
1. Shell形式 
   =/bin/sh= で実行
   ~RUN <コマンド>~
2. Exec形式
   Shellを通さず直接実行
   ~RUN ["実行バイナリ", "パラメータ１", "パラメータ２"]~

* 簡易解説
#+BEGIN_SRC yaml
# 以降の命令で使うベースイメージ
FROM centos:latet

# 生成するイメージのAuther
MAINTAINER K.Mat 

# FROMで指定した、ベースイメージに、アプリやミドルウェアをインストールする、環境構築のためのコマンドを実行する
# Dockerイメージをビルドするときに実行される
# 一行で書けるコマンドはできるだけ一行で書く (キャッシュレイヤの節約)
RUN yum update && RUN yum upgrade
RUN yum -y install\
           httpd\ 
           php\
           php-mbstring\  
           php-pear

# コンテナのプロセス実行時に実行されるコマンド
# Dockerデーモンを呼び出している
# exec形式推奨
CMD ["nginx", "-g", "daemon off;"]

ENTORYPOINT
ONBUILD

VOLUME
LABEL
USER
ARG
STOPSIGNAL
HEALTHCHECK
SHELL

# ホストへの公開ポート
EXPOSE 8080

# dockerfile内で環境変数を指定したい
ENV version="4.0.0"

# イメージ内の作業ディレクトリを決定する (cdみたいな？)
WORKDIR /docker-dir

# ホストにあるtest.htmlをイメージ内の/docker-dir/webに追加する
ADD test.html web/
#+END_SRC
* RUN,CMD,ENTRYPOINT,ONBUILDの違いは？
* ARGとENVの違いは?
* ADDとCOPYの違いは? 
ADDはCOPYの上位互換!
どこが？
- リモートからイメージ内にファイルを追加できる
- アーカイブが自動的に回答される

#+BEGIN_SRC yaml 
ADD <ホストファイルのパス> <Deplopしたい、Dockerイメージのファイルパス>
ADD ["<ホストファイルのパス>" "<Deplopしたい、Dockerイメージのファイルパス>"]
#+END_SRC
[[https://qiita.com/YumaInaura/items/1647e509f83462a37494][Dockerfile の ADD と COPY の違いを結論から書く]]
* 決まりごと
1. Dockerfileはイメージごとにディレクトリ用意し、配置する。
2. Dockerfile用のディレクトリには、Dockerfleの構築に必要なファイルしか置かない。
